"""
### Step 1: Update the header block with comments on your job
### For example: Compute the simple dataset every week

"""
# Leave this section as is. You only need to modify lines where there is a comment on the line
from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.url import URLTouchOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import default_args, dataset, hql_directory, artifact, hadoop_name_node

dag_id = 'my_simple_dag'  #Step 2: Give your job an appropriate name
var_props = VariableProperties(f'{dag_id}_config')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2023, 1, 2)), # Step 3: Set the start date and time
    schedule_interval='@hourly', # Step 4: Set the interval @hourly, @daily, @weekly, @monthly
    tags=['spark', 'hql', 'hive'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    ),
    user_defined_filters=filters,
) as dag:

    # OPTIONAL STEP: If your job relies on the completion of another dataset you can set that dependency here
    # The below example shows how to set this for the update of wmf.webrequest hourly tables
    # To see all the options available for this please go to:
    # https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/analytics/config/datasets.yaml
    # webrequest_sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(dag)

    # Step 5: Provide an appropriate task_id name
    #         Set the path so the code can find your .hql file
    #         Set the query parameters
    etl = SparkSqlOperator(
        task_id='compute_my_query',
        sql=var_props.get('hql_gitlab_raw_path', 
                          'https://gitlab.wikimedia.org/lbowmaker/test_project/-/blob/main/my_simple_query.hql?ref_type=5af37cd55e02244f1162832bf4ecbf2afdd05d85'),
        query_parameters={
            'destination_table': var_props.get('destination_table', 'my_db.my_table'),
            'year': '{{ execution_date.year }}',
            'month': '{{ execution_date.month }}',
        },
        driver_memory='4G',
        executor_memory='8G',
        executor_cores=2,
        conf={
            'spark.dynamicAllocation.maxExecutors': 128,
        }
    )

