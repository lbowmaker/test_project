#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['product_analytics', 'dags', 'my_simple_dag.py']  # Modify as needed


def test_section_topics_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="my_simple_dag")  # Modify as needed
    assert dag is not None
    assert len(dag.tasks) == 4

