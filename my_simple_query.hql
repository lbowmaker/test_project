--Parameters:
--    month    -- year of partition to compute aggregation for.
--    year     -- month of partition to compute aggregation for.
--
-- Usage:
--    spark3-sql -f my_simple_query.hql
--               -d destination_table=my_db.my_table \
--               -d month=2 \
--               -d year=2023


-- Step 1: Copy and paste in your query and add parameters in ${my_param}
-- Step 2: Update the comment block at the top with how you will run your query
INSERT OVERWRITE TABLE ${destination_table}
    SELECT count(*), `database`, year, month 
    FROM event_sanitized.mediawiki_page_create 
    WHERE year = ${year}
    AND month = ${month}
    GROUP BY `database`, year, month;
